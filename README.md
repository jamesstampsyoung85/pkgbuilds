# PKGBUILDs

This repo contains PKGBUILD files for my AUR packages and is managed using [salvador](https://gitlab.com/dpeukert/salvador).

## Copyright

### weatherspect man pages (`weatherspect{,-git}/weatherspect.1`)

Based on the [weatherspect README](https://github.com/AnotherFoxGuy/weatherspect/blob/master/README.md) licensed under [GPLv2](https://github.com/AnotherFoxGuy/weatherspect/blob/master/gpl.txt) (changes: transformed the Markdown readme into the man page format, date: 2019/12/30), therefore licensed under [GPLv2](LICENSE.GPLv2) as well.

### gtk-recordmydesktop files (`gtk-recordmydesktop/{autogen.sh,messages.pot}`)

From the [recordmydesktop repo](https://sourceforge.net/p/recordmydesktop/svn/HEAD/tree/trunk/gtk-recordmydesktop/) and licensed under [GPLv2](https://sourceforge.net/p/recordmydesktop/svn/HEAD/tree/trunk/gtk-recordmydesktop/COPYING).

### qt-recordmydesktop files (`qt-recordmydesktop/{autogen.sh,messages.pot}`)

From the [recordmydesktop repo](https://sourceforge.net/p/recordmydesktop/svn/HEAD/tree/trunk/qt-recordmydesktop/) and licensed under [GPLv3](https://sourceforge.net/p/recordmydesktop/svn/HEAD/tree/trunk/qt-recordmydesktop/COPYING.gpl3).

### All other files

Released into the public domain or where not possible, licensed under the [Unlicense](LICENSE.UNLICENSE).
