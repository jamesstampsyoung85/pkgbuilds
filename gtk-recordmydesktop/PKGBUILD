# Contributor: Jelle van der Waa <jelle@vdwaa.nl>
# Contributor: Daniel J Griffiths <griffithsdj@archlinux.us>
# Contributor: Allan McRae <allan@archlinux.org>
# Contributor: Evangelos Foutras <evangelos@foutrelis.com>
_projectname='recordMyDesktop'
_reponame="${_projectname,,}"
pkgname="gtk-$_reponame"
_pkgver='0.3.8'
_commit='r602'
pkgver="$_pkgver.$_commit"
pkgrel='1'
pkgdesc='GTK2 frontend for recordMyDesktop'
arch=('any')
url="http://$_reponame.sourceforge.net"
license=('GPL3' 'LGPL3')
depends=('gtk2' 'python2>=2.4.0' 'pygtk>=2.10.0' "$_reponame>=0.3.8.1" 'xorg-xwininfo')
makedepends=('autoconf' 'automake>=1.5.0')
source=(
	"$pkgname-$pkgver-$pkgrel.tar.gz::https://downloads.sourceforge.net/project/$_reponame/gtk-$_projectname/$_pkgver/$pkgname-$_pkgver.tar.gz"
	'autogen.sh'
	'messages.pot'
	'r600.diff'
	'r602.diff'
	'selectwindowfix.diff'
	'jack_lsp-check.diff'
	'hebrew.diff'
	'translations1.diff'
	'translations2.diff'
	'typos.diff'
)
sha256sums=(
	'0000000000000000000000000000000000000000000000000000000000000000'
	'0000000000000000000000000000000000000000000000000000000000000000'
	'0000000000000000000000000000000000000000000000000000000000000000'
	'0000000000000000000000000000000000000000000000000000000000000000'
	'0000000000000000000000000000000000000000000000000000000000000000'
	'0000000000000000000000000000000000000000000000000000000000000000'
	'0000000000000000000000000000000000000000000000000000000000000000'
	'0000000000000000000000000000000000000000000000000000000000000000'
	'0000000000000000000000000000000000000000000000000000000000000000'
	'0000000000000000000000000000000000000000000000000000000000000000'
	'0000000000000000000000000000000000000000000000000000000000000000'
)

_sourcedirectory="$pkgname-$_pkgver"

prepare() {
	cd "$srcdir/$_sourcedirectory/"

	# get the files into the state that we would get by downloading a commit snapshot
	# if SourceForge worked properly

	rm -r \
		'aclocal.m4' \
		'configure' \
		'install-sh' \
		'Makefile.in' \
		'missing' \
		'py-compile' \
		'm4/Makefile.in' \
		'src/Makefile.in' \
		'src/rmdConfig.py'

	cp '../messages.pot' 'po/messages.pot'
	cp '../autogen.sh' 'autogen.sh'

	chmod +x 'autogen.sh'

	sed -i 's|#!/usr/bin/python$|#!/usr/bin/env python2.7|g' "src/gtk-$_projectname.in"

	# https://sourceforge.net/p/recordmydesktop/svn/600/
	patch --forward -p2 < "$srcdir/r600.diff"

	# https://sourceforge.net/p/recordmydesktop/svn/602/
	patch --forward -p2 < "$srcdir/r602.diff"

	# https://sources.debian.org/src/gtk-recordmydesktop/0.3.8-4.1/debian/patches/selectwindow.patch/
	patch --forward -p2 < "$srcdir/selectwindowfix.diff"

	# https://sources.debian.org/src/gtk-recordmydesktop/0.3.8-4.1/debian/patches/add-which-check-for-jack_lsp.patch/
	patch --forward -p2 < "$srcdir/jack_lsp-check.diff"

	# https://sources.debian.org/src/gtk-recordmydesktop/0.3.8-4.1/debian/patches/pofiles.patch/
	patch --forward -p2 < "$srcdir/hebrew.diff"

	# https://sources.debian.org/src/gtk-recordmydesktop/0.3.8-4.1/debian/patches/update-translations.patch/
	patch --forward -p2 < "$srcdir/translations1.diff"
	patch --forward -p2 < "$srcdir/translations2.diff"

	# https://bazaar.launchpad.net/~ubuntu-branches/ubuntu/trusty/gtk-recordmydesktop/trusty/view/head:/debian/patches/fix_typos.patch
	patch --forward -p2 < "$srcdir/typos.diff"
}

build() {
	cd "$srcdir/$_sourcedirectory/"

	./autogen.sh
	PYTHON='/usr/bin/python2.7' ./configure --prefix '/usr'
	make
}

package() {
	cd "$srcdir/$_sourcedirectory/"
	make DESTDIR="$pkgdir" install
}
